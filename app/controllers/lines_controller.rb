class LinesController < ApplicationController
  before_action :authenticate_user!, except: [:search, :start]
  before_action :set_line, only: [:show, :edit, :update, :destroy]


  layout 'public', only: [:start, :search]


  # GET /lines
  # GET /lines.json
  def index
    @lines = Line.all
  end

  # GET /lines/1
  # GET /lines/1.json
  def show
  end

  # GET /lines/new
  def new
    @line = Line.new
  end

  # GET /lines/1/edit
  def edit
  end

  # POST /lines
  # POST /lines.json
  def create
    #my_time =  "#{line_params['start_time(4i)']}:#{line_params['start_time(5i)']}"
    @line = Line.new(line_params)
    #@line.start_time = my_time.to_s

    respond_to do |format|
      if @line.save
        format.html { redirect_to @line, notice: 'Line was successfully created.' }
        format.json { render :show, status: :created, location: @line }
      else
        format.html { render :new }
        format.json { render json: @line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lines/1
  # PATCH/PUT /lines/1.json
  def update
    respond_to do |format|
      if @line.update(line_params)
        format.html { redirect_to @line, notice: 'Line was successfully updated.' }
        format.json { render :show, status: :ok, location: @line }
      else
        format.html { render :edit }
        format.json { render json: @line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lines/1
  # DELETE /lines/1.json
  def destroy
    @line.destroy
    respond_to do |format|
      format.html { redirect_to lines_url, notice: 'Line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search

  end

  def start
    if params[:query].present? && params[:destination_id].present?
      dest = Destination.find(params[:destination_id])
      datum = Date.strptime(params[:query].to_s, '%m/%d/%Y')
      day = datum.strftime('%A')
      day_id = Day.find_by_name(day.to_s).id
      @lines = dest.lines
      @lines = @lines.where(day_id: day_id)

      @lines = @lines.where(carrier_id: params[:carrier_id]) if !params[:carrier_id].blank?
      @lines = @lines.where(line_type_id: params[:line_type_id]) if !params[:line_type_id].blank?
    else
      @lines = nil
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line
      @line = Line.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_params
      # params.require(:line).permit(:cost, :line_type_id, :carrier_id, :day_id, :destination_id, :start_time,
      #                              destinations_lines_attributes: [:id, :_destroy, :destination_id,
      #                                                              destination_attributes: [:id, :name, :_destroy]])
      params.require(:line).permit(:cost, :line_type_id, :carrier_id, :day_id, :start_time, :destination_tokens)
    end
end
