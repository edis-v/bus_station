json.array!(@lines) do |line|
  json.extract! line, :id, :cost, :line_type_id, :carrier_id, :day, :destination_id, :start_time
  json.url line_url(line, format: :json)
end
