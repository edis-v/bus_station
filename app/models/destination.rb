class Destination < ActiveRecord::Base
  has_many :destinations_lines
  has_many :lines, through: :destinations_lines

  def self.tokens(query)
    destinations = where("name like ?", "%#{query.titleize}%")
    if destinations.empty?
      [{id: "<<<#{query}>>>", name: "New \"#{query}\""}]
    else
      destinations
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1.titleize).id}
    tokens.split(',')
  end

end
