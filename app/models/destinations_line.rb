class DestinationsLine < ActiveRecord::Base
  belongs_to :line
  belongs_to :destination
  accepts_nested_attributes_for :destination, reject_if: :all_blank
end
