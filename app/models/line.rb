class Line < ActiveRecord::Base
  belongs_to :line_type
  belongs_to :carrier
  belongs_to :day
  has_many :destinations_lines, :dependent => :destroy
  has_many :destinations, through: :destinations_lines
  # accepts_nested_attributes_for :destinations_lines, :allow_destroy => true
  # accepts_nested_attributes_for :destinations

  def destination_tokens
    self.destination_ids
  end

  def destination_tokens=(tokens)
    self.destination_ids = Destination.ids_from_tokens(tokens)
  end


end
