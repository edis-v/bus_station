# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:load', ->
  $('#line_destination_tokens').tokenInput('/destinations.json', {
    theme: 'facebook'
    prePopulate: $('#line_destination_tokens').data('load')
  })