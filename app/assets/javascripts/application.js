// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require jquery-ui
//= require jquery.tokeninput
//= require cocoon
//= require turbolinks
//= require_tree .


$("#destinations a.add_fields").
    data("association-insertion-position", 'before').
    data("association-insertion-node", 'this');

$('#destinations').on('cocoon:after-insert',
    function() {
        $(".destinations_line-fields a.add_fields").
            data("association-insertion-position", 'before').
            data("association-insertion-node", 'this');
        $('.destinations_line-fields').on('cocoon:after-insert',
            function() {
                $(this).children("#destination_from_list").remove();
                $(this).children("a.add_fields").hide();
            });
    });

$(function() {
    $('.datepicker').datepicker();
});