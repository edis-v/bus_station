# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


['Monday', 'Tuesday', 'Wednesday','Thursday', 'Friday', 'Saturday', 'Sunday'].each do |day|
  Day.where(name: day).first_or_create
end

['Local', 'State', 'International'].each do |type|
  LineType.where(name: type).first_or_create
end

['Globtour', 'Centrotours', 'Muric trans'].each do |carrier|
  Carrier.where(name: carrier).first_or_create
end

['Cazin', 'Bihać', 'Sarajevo', 'Polje', 'Zagreb', 'Minhen'].each do |city|
  Destination.where(name: city).first_or_create
end