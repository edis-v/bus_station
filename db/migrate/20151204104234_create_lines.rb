class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.integer :cost
      t.belongs_to :line_type, index: true, foreign_key: true
      t.belongs_to :carrier, index: true, foreign_key: true
      t.belongs_to :day
      t.belongs_to :destination, index: true, foreign_key: true
      t.string :start_time

      t.timestamps null: false
    end
  end
end
