class CreateDestinationsLines < ActiveRecord::Migration
  def change
    create_table :destinations_lines do |t|
      t.belongs_to :line, index: true, foreign_key: true
      t.belongs_to :destination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
