class RemoveDestinationFromLines < ActiveRecord::Migration
  def change
    remove_column :lines, :destination_id
  end
end
